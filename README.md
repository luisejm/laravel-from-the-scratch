# __Curso de Laravel from the scratch__
## Introducción
Bienvenidos al curso de Laravel from the scratch, a continuación iremos describiendo cada una de las secciones.

## __Sección 1: Prerequisitos e inicio__

### __Instalación de paquetes__
Debemos ingresar a Homebrew para poder instalarlo en Mac, para ello vamos al su página principal [Homebrew](https://brew.sh/index_es),seguidamente aplicamos los pasos correspondientes:
1. __Verificar e instalar php:__ Abrimos la terminal y verificamos si tenemos php instalado con el siguiente comando 
> *php -v*

Sí no tenemos php instalado, entonces debemos ejecutar el siguiente comando para instalar Brew:
> /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

Gracias a esto podremos instalar diferentes paquetes, entre ellos php:
> brew install php

2. __Instalación de mysql:__ En nuestra terminal debemos ejecutar los siguientes comandos:
> brew install mysql

3. __Instalación de composer:__ Ingresamos a la web principal de Composer [AQUÍ](https://getcomposer.org/download/). Copiamos y ejecutamos el siguiente comando en nuestra terminal para descargar el archivo *composer.phar*.
> php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

Seguidamente, debemos mover ese archivo a la carpeta contenedora:
> mv composer.phar /usr/local/bin/composer

### __Creación del proyecto Lavarel con Composer__
Ejecutamos el siguiente comando para crear nuestro primer proyecto:
> composer create-project laravel/laravel lfts

Finalmente, para comprobar que nuestro proyecto se ha instalado de la manera correcta, ejecutamos el comando que inicia el servidor:
>php artisan serve

## __Sección 2: Conceptos básicos__

### __Aplicación de hojas de estilo en cascada__
Dentro de nuestro proyecto laravel, podemos utilizar CSS, y para esto lo que tenemos que hacer es crear un archivo .css y en él agregar los estilos que deseemos, ahora bien, para poder incorporarlos en nuestra aplicació inicial debemos utilizar la siguiente linea en nuestra vista:
> link rel="stylesheet" href="/app.css"

Esto nos permitirá hacer un enlace directamente al archivo css que deseamos utilizar.

Lo mismo sucede con los scripts pero en este caso la manera de importarlo sería la siguiente:
> script src="/app.js

### __Creación de nuevas rutas__
Cuando deseamos crear nuevas vistas, necesitaremos primero definir como llegar a ella mediante las rutas, y para lograr eso debemos definir dicha ruta en el archivo *__routes/web.php__*, dentro de este colocamos algo como lo siguiente:
> Route::get('post', function () {
    return view('post');
});

### __Envío de parámetros por rutas__
Durante el uso de nuestra página web deseamos enviar datos en la navegación como parámetros, para ello, podemos modificar el archivo de rutas, donde incluiremos la siguiente linea de código, en la cual definimos que información deseamos enviar:
>'post' => file_get_contents(__DIR__ . '/../resources/posts/primer_post.html')

Como podemos apreciar, luego de la variable DIR, debemos definir el directorio donde se encuentra la información que enviaremos.




